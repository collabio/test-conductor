"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
//import express = require('express');
const io = require("socket.io-client");
//var app = express();
//app.listen(process.env.port || 3000);
let clients = [];
class Client {
    constructor(url) {
        this.url = url;
    }
    Start(type, protocol, server) {
        this.finished = false;
        this.results = null;
        this.onFinish = null;
        var socket = io(this.url);
        socket.emit('test', { type, protocol, server, peers: clients.length - 1 });
        socket.on('results', (results) => {
            this.dispose = () => {
                socket.emit('dispose');
                socket.close();
            };
            this.results = results;
            this.finished = true;
            if (this.onFinish)
                this.onFinish(results);
        });
    }
    Dispose() {
        if (this.dispose)
            this.dispose();
        this.dispose = null;
    }
    Finished() {
        return new Promise(resolved => {
            if (this.finished)
                resolved(this.results);
            this.onFinish = resolved;
        });
    }
}
(() => __awaiter(this, void 0, void 0, function* () {
    var clientServers = process.argv.slice(5);
    for (let server of clientServers)
        clients.push(new Client(server));
    for (let client of clients)
        client.Start(process.argv[2], process.argv[3], process.argv[4]);
    let results = [];
    for (let client of clients)
        results.push(yield client.Finished());
    for (let client of clients)
        client.Dispose();
    process.stdout.write(JSON.stringify(results));
}))();
