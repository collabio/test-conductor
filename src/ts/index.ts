//import express = require('express');
import * as io from 'socket.io-client';

//var app = express();
//app.listen(process.env.port || 3000);

let clients = [];

class Client {
    protected onFinish: (any) => void;
    protected finished: boolean;
    protected results: any;
    protected dispose: () => void;

    public constructor(protected url) {

    }

    public Start(type: string, protocol: string, server: string) {
        this.finished = false;
        this.results = null;
        this.onFinish = null;

        var socket = io(this.url);

        socket.emit('test', { type, protocol, server, peers: clients.length - 1 });
        socket.on('results', (results) => {

            this.dispose = () => {
                socket.emit('dispose');
                socket.close();
            };

            this.results = results;
            this.finished = true;
            if (this.onFinish)
                this.onFinish(results);
        });
    }

    public Dispose() {
        if (this.dispose)
            this.dispose();
        this.dispose = null;
    }

    public Finished() {
        return new Promise<void>(resolved => {
            if (this.finished)
                resolved(this.results);
            this.onFinish = resolved;
        });
    }
}

(async () => {
    var clientServers = process.argv.slice(5);

    for(let server of clientServers)
        clients.push(new Client(server));

    for (let client of clients)
        client.Start(process.argv[2], process.argv[3], process.argv[4]);

    let results = [];
    for (let client of clients)
        results.push(await client.Finished());

    for (let client of clients)
        client.Dispose();

    process.stdout.write(JSON.stringify(results));
})();